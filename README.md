You can read the full article on my blog: https://blog.jytou.fr/2019/11/13/a-board-for-paper-based-spaced-repetition-memorization/

# 3D Paper Spaced Repetition

There are many spaced repetition algorithms and software out there. But physical devices to use spaced repetition are not being developed. This offers a 3d printed board which can be used to do spaced repetition with simple paper pieces.

# Print it!

First, print the board, as well as 7 pegs. I advise to use a different color for the pegs so that they are clearly visible. I used white for the board and red for the pegs.

Then small 2x2 cm square pieces of paper are used to fill the board.

![Printed Board](3dspacedrep.jpg)

# Spaced Repetition

Just as a reminder, spaced repetition works in such a way that what you are exposing yourself to what you are learning at growing interval. This board helps dealing with those growing intervals. Every delimited space on the board corresponds to a specific interval. Besides, a series of “pegs” also needs to be printed: they show the current date for every section of the board.

# Meaning of columns

The board starts at the bottom left. The first column on the left contains 3 different areas: daily repeat (today, tomorrow), in 4 days, in 7 days. Then, the next columns represent: 30 days, 50 days, 50 weeks, 50 months.

# Add some marks on the board

The first sections are easy: each peg simply needs to be moved forward once a day, and then loop back to the beginning when it reaches the end of the series. However, the two last columns are a little trickier: the peg should be moved every week for the first and every month for the second.

So to help with that, we can simply mark the column in which we have 50 days with 2 different colors: one every week (roughly every 7 days) and one every month (there are 50 days, so we can make it every 25 days).

# Practically

Every day, simply move the pegs forward: all of them move one space forward, except for the two last columns, which move only when the peg in the 3<sup>rd</sup> column reaches one of the marked places. Then review the papers that are in the places marked by the pegs, starting from the left column. Every time you do remember a card, that paper moves to the next column. Every time you fail to remember a card, that card goes back to the beginning. It's as simple as that!

# Reverse cards

Cards that can be learned from both sides can be used in 2 different ways: either just print them twice - front/back and back/front, either just turn them after a review so that the back side becomes the front side the next time that card is reviewed. A special “forbidden” sign can be inserted at the back of cards that are only one way.
