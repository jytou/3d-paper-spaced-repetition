raill=150;
railtop=25;
railbottom=30;

module prism(l, w, h)
{
       polyhedron(
               points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]],
               faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
               );
}

module column(ll, ww, nn, hh)
{
	l=(ll-2)/nn-2;
	for (x = [1:nn])
		translate([2+(l+2)*(x-1), 0, 1]) cube([l, ww, hh]);
}

thick = 2.2;
rr1 = 8.2;
rr2 = 6.2;

module peg()
{
	scale([1, 0.5, 1]) rotate([0, 90, 0]) cylinder(h=thick, r1=rr1, r2=rr2);
}

module pegcol(ll, nn)
{
	//l=(ll-2-0.8)/nn-1;
	l = (ll-4-nn*thick)/(nn-1)+thick;
	for (x = [1:nn])
		translate([2+l*(x-1), 0, 0]) peg();
}

hh=15;
inter=32;
widcard=20;

difference()
{
	cube([180, 162, hh]);
	for (y = [0:2])
		translate([0, 2+y*inter, 0]) column(180, widcard, 20, hh);
	translate([0, 27, hh]) pegcol(180, 50);
	translate([0, 27+inter, hh]) pegcol(180, 50);
	translate([0, 27+inter*2, hh]) pegcol(180, 50);
	translate([0, 2+3*inter, 0]) column(180, widcard, 30, hh);
	translate([0, 27+inter*3, hh]) pegcol(180, 30);

	translate([0, 2+4*inter, 0]) column(100, widcard, 14, hh);
	translate([0, 27+inter*4, hh]) pegcol(100, 14);
	translate([108, 2+4*inter, 0]) column(40, widcard, 4, hh);
	translate([108, 27+inter*4, hh]) pegcol(40, 4);
	translate([155, 2+4*inter, 0]) column(25, widcard, 2, hh);
	translate([155, 27+inter*4, hh]) pegcol(25, 2);
}
