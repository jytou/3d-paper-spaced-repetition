module tore(smallr, bigr)
{
    rotate_extrude() translate([bigr, 0, 0]) circle(r=smallr);
}

module peg(thick, rr1, rr2, rr3, ss)
{
	union()
	{
		cylinder(h=thick, r1=rr1, r2=rr2);
		difference()
		{
			scale([ss, 1, 1]) cylinder(h=thick, r=rr3);
			translate([-20, 0, -0.5]) cube([40, 40, thick+1]);
		}
	}
}

thick=1;

$fn=100;
scale([0.5, 1, 1]) union()
{
	//peg(2, 8, 6, 9, 1);
	translate([0, 30, 0]) peg(3, 8, 6, 13, 0.7);
	//translate([0, 60, 0]) peg(3.9, 8, 6, 15, 0.7);
}